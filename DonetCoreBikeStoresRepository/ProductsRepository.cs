﻿using EFCoreBikeStores.Models;
using System;
using System.Collections.Generic;
using System.Text;
using DonetCoreBikeStoresRepository.Contracts;

namespace DonetCoreBikeStoresRepository
{
  public class ProductsRepository: RepositoryBase<Products>, IProductsRepository
  {
    public ProductsRepository(BikeStoresContext context)
           : base(context)
    {
    }
  }
}
