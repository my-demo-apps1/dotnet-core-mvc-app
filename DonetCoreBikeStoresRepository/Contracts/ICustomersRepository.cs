﻿using EFCoreBikeStores.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DonetCoreBikeStoresRepository.Contracts
{
  public interface ICustomersRepository : IRepositoryBase<Customers>
  {
  }
}
