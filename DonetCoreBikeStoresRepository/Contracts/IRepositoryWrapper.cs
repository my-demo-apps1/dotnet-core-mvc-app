﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DonetCoreBikeStoresRepository.Contracts
{
 public interface IRepositoryWrapper
 {
  IProductsRepository Products { get; }
  ICustomersRepository Customers { get; }
  IOrdersRepository Orders { get; }
  IOrderItemsRepository OrderItems { get; }
  void Save();
 }
}
