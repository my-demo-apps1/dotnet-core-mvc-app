﻿using DonetCoreBikeStoresRepository.Contracts;
using EFCoreBikeStores.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DonetCoreBikeStoresRepository
{
 public class OrdersRepository : RepositoryBase<Orders>, IOrdersRepository
 {
  public OrdersRepository(BikeStoresContext context)
        : base(context)
  {
  }
 }
}
