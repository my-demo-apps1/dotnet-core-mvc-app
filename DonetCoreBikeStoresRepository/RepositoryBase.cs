﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using DonetCoreBikeStoresRepository.Contracts;
using EFCoreBikeStores.Models;
using Microsoft.EntityFrameworkCore;

namespace DonetCoreBikeStoresRepository
{
  public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : class
  {
    protected BikeStoresContext _context;

    public RepositoryBase(BikeStoresContext context)
    {
      this._context = context;
    }

    public IQueryable<T> FindAll()
    {
      return this._context.Set<T>().AsNoTracking();
    }

    public IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression)
    {
      return this._context.Set<T>().Where(expression).AsNoTracking();
    }

    public void Create(T entity)
    {
      this._context.Set<T>().Add(entity);
    }

    public void Update(T entity)
    {
      this._context.Set<T>().Update(entity);
    }

    public void Delete(T entity)
    {
      this._context.Set<T>().Remove(entity);
    }
  }
}
