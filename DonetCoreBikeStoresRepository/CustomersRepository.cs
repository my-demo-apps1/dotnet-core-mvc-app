﻿using DonetCoreBikeStoresRepository.Contracts;
using EFCoreBikeStores.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DonetCoreBikeStoresRepository
{
  public class CustomersRepository : RepositoryBase<Customers>, ICustomersRepository
  {
    public CustomersRepository(BikeStoresContext context)
          : base(context)
    {
    }
  }
}
