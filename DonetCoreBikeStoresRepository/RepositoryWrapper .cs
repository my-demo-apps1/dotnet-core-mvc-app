﻿using DonetCoreBikeStoresRepository.Contracts;
using EFCoreBikeStores.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DonetCoreBikeStoresRepository
{
 public class RepositoryWrapper : IRepositoryWrapper
 {
  private BikeStoresContext _context;
  private IProductsRepository _products;
  private ICustomersRepository _customers;
  private IOrdersRepository _orders;
  private IOrderItemsRepository _orderItems;

  public IProductsRepository Products
  {
   get
   {
    if (_products == null)
    {
     _products = new ProductsRepository(this._context);
    }

    return _products;
   }
  }

  public ICustomersRepository Customers
  {
   get
   {
    if (_customers == null)
    {
     _customers = new CustomersRepository(this._context);
    }

    return _customers;
   }
  }

  public IOrdersRepository Orders
  {
   get
   {
    if (_orders == null)
    {
     _orders = new OrdersRepository(this._context);
    }

    return _orders;
   }
  }

  public IOrderItemsRepository OrderItems
  {
   get
   {
    if (_orderItems == null)
    {
     _orderItems = new OrderItemsRepository(this._context);
    }

    return _orderItems;
   }
  }

  public RepositoryWrapper(BikeStoresContext context)
  {
   this._context = context;
  }

  public void Save()
  {
   this._context.SaveChanges();
  }
 }
}
