﻿using DonetCoreBikeStoresRepository.Contracts;
using EFCoreBikeStores.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DonetCoreBikeStoresRepository
{
 public class OrderItemsRepository : RepositoryBase<OrderItems>, IOrderItemsRepository
 {
  public OrderItemsRepository(BikeStoresContext context)
        : base(context)
  {
  }
 }
}
