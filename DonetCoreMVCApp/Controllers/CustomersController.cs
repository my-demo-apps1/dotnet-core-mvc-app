﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EFCoreBikeStores.Models;
using DonetCoreBikeStoresRepository.Contracts;
using System.Diagnostics;
using DonetCoreMVCApp.Models;
using Microsoft.AspNetCore.Http;

namespace DonetCoreMVCApp.Controllers
{
 public class CustomersController : Controller
 {
  private readonly IRepositoryWrapper _repoWrapper;

  public CustomersController(IRepositoryWrapper repoWrapper)
  {
   _repoWrapper = repoWrapper;
  }

  // GET: Customers
  public async Task<IActionResult> Index()
  {
   int? customerId = Request.HttpContext.Session.GetInt32("userId");
   if (customerId.HasValue && customerId.Value > 0)
   {
    return RedirectToAction("Details", new { id = customerId });
   }
   else
   {
    return View(_repoWrapper.Customers.FindAll());
   }
  }

  public async Task<IActionResult> Impersonate(int? id)
  {
   int customerId = (id.HasValue ? id.Value : -1);
   Request.HttpContext.Session.SetInt32("userId", customerId);
   return RedirectToAction("Details", new { id = id });
  }

  public async Task<IActionResult> StopImpersonate()
  {
   Request.HttpContext.Session.SetInt32("userId", -1);
   return RedirectToAction("Index");
  }


  // GET: Customers/Details/5
  public async Task<IActionResult> Details(int? id)
  {
   if (!id.HasValue)
   {
    return NotFound();
   }

   var customers =  _repoWrapper.Customers.FindByCondition(m => m.CustomerId == id)
    .Include(c => c.Orders)
    .ThenInclude(co => co.OrderItems)
    .ThenInclude(cop => cop.Product).ToList<Customers>();

   if (customers == null || customers.Count < 1 )
   {
    return NotFound();
   }
   return View(customers[0]);
  }

  [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
  public IActionResult Error()
  {
   return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
  }
 }
}
