﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EFCoreBikeStores.Models;
using DonetCoreBikeStoresRepository.Contracts;

namespace DonetCoreMVCApp.Controllers
{
  public class ProductsController : Controller
  {
    private readonly IRepositoryWrapper _repoWrapper;

    public ProductsController(IRepositoryWrapper repoWrapper)
    {
      _repoWrapper = repoWrapper;
    }

    // GET: Products
    public async Task<IActionResult> Index()
    {
      var products = _repoWrapper.Products.FindAll().Include(p => p.Brand).Include(p => p.Category).AsQueryable();
      return View(products);
    }

    public PartialViewResult ListingTable()
    {
      var products = _repoWrapper.Products.FindAll().Include(p => p.Brand).Include(p => p.Category).AsQueryable();
      return PartialView("_ListingTable", products);
    }

    public PartialViewResult ListingTiles(int? id)
    {
      if(id == null)
      {
        id = 1;
      }
      var products = _repoWrapper.Products.FindAll().Include(p => p.Brand).Include(p => p.Category).AsQueryable();
      return PartialView("_ListingTiles", products.Take(12 * id.Value));
    }
    //// GET: Products/Details/5
    //public async Task<IActionResult> Details(int? id)
    //{
    //    if (id == null)
    //    {
    //        return NotFound();
    //    }

    //    var products = await _context.Products
    //        .Include(p => p.Brand)
    //        .Include(p => p.Category)
    //        .FirstOrDefaultAsync(m => m.ProductId == id);
    //    if (products == null)
    //    {
    //        return NotFound();
    //    }

    //    return View(products);
    //}

    //// GET: Products/Create
    //public IActionResult Create()
    //{
    //    ViewData["BrandId"] = new SelectList(_context.Brands, "BrandId", "BrandName");
    //    ViewData["CategoryId"] = new SelectList(_context.Categories, "CategoryId", "CategoryName");
    //    return View();
    //}

    //// POST: Products/Create
    //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    //[HttpPost]
    //[ValidateAntiForgeryToken]
    //public async Task<IActionResult> Create([Bind("ProductId,ProductName,BrandId,CategoryId,ModelYear,ListPrice")] Products products)
    //{
    //    if (ModelState.IsValid)
    //    {
    //        _context.Add(products);
    //        await _context.SaveChangesAsync();
    //        return RedirectToAction(nameof(Index));
    //    }
    //    ViewData["BrandId"] = new SelectList(_context.Brands, "BrandId", "BrandName", products.BrandId);
    //    ViewData["CategoryId"] = new SelectList(_context.Categories, "CategoryId", "CategoryName", products.CategoryId);
    //    return View(products);
    //}

    //// GET: Products/Edit/5
    //public async Task<IActionResult> Edit(int? id)
    //{
    //    if (id == null)
    //    {
    //        return NotFound();
    //    }

    //    var products = await _context.Products.FindAsync(id);
    //    if (products == null)
    //    {
    //        return NotFound();
    //    }
    //    ViewData["BrandId"] = new SelectList(_context.Brands, "BrandId", "BrandName", products.BrandId);
    //    ViewData["CategoryId"] = new SelectList(_context.Categories, "CategoryId", "CategoryName", products.CategoryId);
    //    return View(products);
    //}

    //// POST: Products/Edit/5
    //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    //[HttpPost]
    //[ValidateAntiForgeryToken]
    //public async Task<IActionResult> Edit(int id, [Bind("ProductId,ProductName,BrandId,CategoryId,ModelYear,ListPrice")] Products products)
    //{
    //    if (id != products.ProductId)
    //    {
    //        return NotFound();
    //    }

    //    if (ModelState.IsValid)
    //    {
    //        try
    //        {
    //            _context.Update(products);
    //            await _context.SaveChangesAsync();
    //        }
    //        catch (DbUpdateConcurrencyException)
    //        {
    //            if (!ProductsExists(products.ProductId))
    //            {
    //                return NotFound();
    //            }
    //            else
    //            {
    //                throw;
    //            }
    //        }
    //        return RedirectToAction(nameof(Index));
    //    }
    //    ViewData["BrandId"] = new SelectList(_context.Brands, "BrandId", "BrandName", products.BrandId);
    //    ViewData["CategoryId"] = new SelectList(_context.Categories, "CategoryId", "CategoryName", products.CategoryId);
    //    return View(products);
    //}

    //// GET: Products/Delete/5
    //public async Task<IActionResult> Delete(int? id)
    //{
    //    if (id == null)
    //    {
    //        return NotFound();
    //    }

    //    var products = await _context.Products
    //        .Include(p => p.Brand)
    //        .Include(p => p.Category)
    //        .FirstOrDefaultAsync(m => m.ProductId == id);
    //    if (products == null)
    //    {
    //        return NotFound();
    //    }

    //    return View(products);
    //}

    //// POST: Products/Delete/5
    //[HttpPost, ActionName("Delete")]
    //[ValidateAntiForgeryToken]
    //public async Task<IActionResult> DeleteConfirmed(int id)
    //{
    //    var products = await _context.Products.FindAsync(id);
    //    _context.Products.Remove(products);
    //    await _context.SaveChangesAsync();
    //    return RedirectToAction(nameof(Index));
    //}

    //private bool ProductsExists(int id)
    //{
    //    return _context.Products.Any(e => e.ProductId == id);
    //}
  }
}
