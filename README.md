# dotnet-core-mvc-app

dotnet-core-mvc-app is a demo ASP.NET MVC Web Application. It uses 
* ASP.NET MVC Frontend
* EntityFramework 
* Repository Pattern 
* Unit Tests
* Bootstrap
* Jquery
* SQL Server Databaase
* Hosted on Amazon Web Services

# Amazon Web Services
* Web Application is hosted on EC2
* SQL Server Database is hosted on RDS
* Used ElasticBeanStalk for provisioning EC2 manchine and Web server
